<?php

/**
 * Plugin declaration.
 */
$plugin = array(
  'process callback' => 'yandex_most_popular_response_extract_nodes',
  'name' => 'Node',
);

/**
 * Prepare Yandex Metrika response. Convert it to entity array.
 */
function yandex_most_popular_response_extract_nodes($data, $period) {
  $unsorted = array();
  foreach ($data as $row) {
    $url = explode('/', $row->url, 4);
    if ($url[3] != '') {
      $unsorted[$url[3]] = array('views' => $row->page_views);
    }
  }
  $q = db_select('url_alias','u');
  $q->leftJoin('node', 'n', 'u.source=CONCAT(\'node/\', n.nid)');
  $q->fields('u', array('alias', 'source'))
    ->fields('n', array('nid'))
    ->condition('n.status', 1)
    ->condition('u.alias', array_keys($unsorted));
  $res = $q->execute();
  while($r = $res->fetchAssoc()) {
    $unsorted[$r['alias']]['source'] = $r['source'];
    $unsorted[$r['alias']]['entity_id'] = $r['nid'];
    $unsorted[$r['alias']]['entity_type'] = 'node';
    $unsorted[$r['alias']]['period'] = $period;
  }
  $sorted = array();
  foreach ($unsorted as $url => $row) {
    if ((isset($row['entity_id'])) && (isset($row['source'])) && (substr($row['source'], 0, 5) == 'node/')) {
      $sorted['node_' . $row['entity_id'] . '_' . $period] = $row;
    }
  }
  $sorted = array_values($sorted);
  return $sorted;
}
