<?php

/**
 * Plugin declaration.
 */
$plugin = array(
  'process callback' => 'yandex_most_popular_response_extract_terms',
  'name' => 'Taxonomy term'
);

/**
 * Prepare Yandex Metrika response. Convert it to entity array.
 */
function yandex_most_popular_response_extract_terms($data, $period) {
  $unsorted = array();
  foreach ($data as $row) {
    $url = explode('/', $row->url, 4);
    if($url[3] != '') {
      $unsorted[$url[3]] = array('views' => $row->page_views);
    }
  }
  $q = db_select('url_alias','u');
  $q->leftJoin('taxonomy_term_data', 't', 'u.source=CONCAT(\'taxonomy/term/\', t.tid)');
  $q->fields('u', array('alias', 'source'))
    ->fields('t', array('tid'))
    ->condition('u.alias', array_keys($unsorted));
  $res = $q->execute();
  while($r = $res->fetchAssoc()) {
    $unsorted[$r['alias']]['source'] = $r['source'];
    $unsorted[$r['alias']]['entity_id'] = $r['tid'];
    $unsorted[$r['alias']]['entity_type'] = 'term';
    $unsorted[$r['alias']]['period'] = $period;
  }
  $sorted = array();
  foreach ($unsorted as $url => $row) {
    if ((isset($row['entity_id'])) && (isset($row['source'])) && (substr($row['source'], 0, 14) == 'taxonomy/term/')) {
      $sorted['term_' . $row['entity_id'] . '_' . $period] = $row;
    }
  }
  $sorted = array_values($sorted);
  return $sorted;
}
