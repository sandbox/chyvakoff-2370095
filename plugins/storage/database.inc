<?php

/**
 * Plugin declaration.
 */
$plugin = array(
  'storage set callback' => 'yandex_most_popular_storage_database_set',
  'storage get callback' => 'yandex_most_popular_storage_database_get',
  'name' => 'Database',
);

/**
 * Save statistic to database.
 */
function yandex_most_popular_storage_database_set($data) {
  try {
    // Insert new statistics.
    foreach ($data as $entity) {
      db_insert('yandex_most_popular_entity')
        ->fields(
          array(
            'entity_id' => $entity['entity_id'],
            'entity_type' => $entity['entity_type'],
            'views' => $entity['views'],
            'period' => $entity['period']
          )
        )
        ->execute();
    }
    return TRUE;
  } catch (Exception $e) {
    watchdog_exception('yandex_most_popular', $e);
    return FALSE;
  }
}

/**
 * Get rows from database.
 */
function yandex_most_popular_storage_database_get($entity_type = 'all', $period = 'all', $count = 'all') {
  $q = db_select('yandex_most_popular_entity', 'yme')
    ->fields('yme', array('entity_id', 'entity_type', 'views', 'period'))
    ->orderBy('yme.views', 'desc');
  if ($entity_type != 'all') {
    $q->condition('yme.entity_type', $entity_type);
  }
  if ($period != 'all') {
    $q->condition('yme.period', $period);
  }
  if ($count != 'all') {
    $q->range(0, $count);
  }
  $res = $q->execute();
  $output = array();
  while($row = $res->fetchAssoc()) {
    $output[] = $row;
  }
  return $output;
}
