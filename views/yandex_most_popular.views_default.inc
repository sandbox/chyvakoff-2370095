<?php

/**
 * @file
 * Provide default views data for yandex_most_popular.module.
 */

function yandex_most_popular_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'yandex_most_popular_nodes';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Yandex most popular nodes';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Yandex most popular nodes';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'yandex_most_popular_entity' => 'yandex_most_popular_entity',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'yandex_most_popular_entity' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Content: Most popular content */
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['table'] = 'node';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Yandex most popular */
  $handler->display->display_options['fields']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['fields']['yandex_most_popular_entity']['table'] = 'node';
  $handler->display->display_options['fields']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  /* Sort criterion: Content: Yandex most popular */
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['table'] = 'node';
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Yandex most popular */
  $handler->display->display_options['filters']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['table'] = 'node';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['operator'] = '>';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['value'] = '0';

  /* Display: Most popular by day */
  $handler = $view->new_display('block', 'Most popular by day', 'block');

  /* Display: Most popular by week */
  $handler = $view->new_display('block', 'Most popular by week', 'block_1');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Most popular content */
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['table'] = 'node';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['required'] = TRUE;
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['period'] = 'week';

  /* Display: Most popular by month */
  $handler = $view->new_display('block', 'Most popular by month', 'block_2');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Most popular content */
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['table'] = 'node';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['required'] = TRUE;
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['period'] = 'month';
  $translatables['yandex_most_popular_nodes'] = array(
    t('Master'),
    t('Yandex most popular nodes'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Yandex most popular entities.'),
    t('Title'),
    t('Yandex most popular'),
    t('Most popular by day'),
    t('Most popular by week'),
    t('Most popular by month'),
  );
  $views[$view->name] = $view;

  $view = new view();
  $view->name = 'yandex_most_popular_terms';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Yandex most popular terms';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Yandex most popular terms';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'yandex_most_popular_entity' => 'yandex_most_popular_entity',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'yandex_most_popular_entity' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Taxonomy term: Most popular taxonomy terms */
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['required'] = TRUE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Field: Taxonomy term: Yandex most popular */
  $handler->display->display_options['fields']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['fields']['yandex_most_popular_entity']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  /* Sort criterion: Taxonomy term: Yandex most popular */
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['sorts']['yandex_most_popular_entity']['order'] = 'DESC';
  /* Filter criterion: Taxonomy term: Yandex most popular */
  $handler->display->display_options['filters']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['operator'] = '>';
  $handler->display->display_options['filters']['yandex_most_popular_entity']['value'] = '0';

  /* Display: Most popular by day */
  $handler = $view->new_display('block', 'Most popular by day', 'block');

  /* Display: Most popular by week */
  $handler = $view->new_display('block', 'Most popular by week', 'block_1');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Taxonomy term: Most popular taxonomy terms */
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['required'] = TRUE;
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['period'] = 'week';

  /* Display: Most popular by month */
  $handler = $view->new_display('block', 'Most popular by month', 'block_2');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Taxonomy term: Most popular taxonomy terms */
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['id'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['field'] = 'yandex_most_popular_entity';
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['required'] = TRUE;
  $handler->display->display_options['relationships']['yandex_most_popular_entity']['period'] = 'month';
  $translatables['yandex_most_popular_terms'] = array(
    t('Master'),
    t('Yandex most popular terms'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Yandex most popular entities.'),
    t('Name'),
    t('Yandex most popular'),
    t('Most popular by day'),
    t('Most popular by week'),
    t('Most popular by month'),
  );
  $views[$view->name] = $view;

  return $views;
}