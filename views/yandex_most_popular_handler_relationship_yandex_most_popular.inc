<?php
/**
 * @file
 * Implements the relationship handler class.
 */

class yandex_most_popular_handler_relationship_yandex_most_popular extends views_handler_relationship {
  function admin_summary() {
    return 'by ' . $this->options['period'] . '';
  }

  function option_definition() {
    $options = parent::option_definition();

    // Relationships definitions should define a default label, but if they aren't get another default value.
    if (!empty($this->definition['label'])) {
      $label = $this->definition['label'];
    }
    else {
      $label = !empty($this->definition['field']) ? $this->definition['field'] : $this->definition['base field'];
    }

    $options['label'] = array('default' => $label, 'translatable' => TRUE);
    $options['required'] = array('default' => FALSE, 'bool' => TRUE);
    $options['period'] = array('default' => 'day');

    return $options;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Identifier'),
      '#default_value' => isset($this->options['label']) ? $this->options['label'] : '',
      '#description' => t('Edit the administrative label displayed when referencing this relationship from filters, etc.'),
      '#required' => TRUE,
    );

    $form['required'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require this relationship'),
      '#description' => t('Enable to hide items that do not contain this relationship'),
      '#default_value' => !empty($this->options['required']),
    );

    $options = array();
    $periods = explode("\n", variable_get('yandex_most_popular_periods', "day\nweek\nmonth"));
    foreach ($periods as $period) {
      $options[trim($period)] = trim($period);
    }

    $form['period'] = array(
      '#type' => 'select',
      '#title' => t('Period'),
      '#description' => t('Views for period.'),
      '#options' => $options,
      '#default_value' => $this->options['period'],
    );
  }

  /**
   * Called to implement a relationship in a query.
   */
  function query() {
    // Figure out what base table this relationship brings to the party.
    $table_data = views_fetch_data($this->definition['base']);
    $base_field = empty($this->definition['base field']) ? $table_data['table']['base']['field'] : $this->definition['base field'];

    $this->ensure_my_table();

    $def = $this->definition;
    $def['table'] = $this->definition['base'];
    $def['field'] = $base_field;
    $def['left_table'] = $this->table_alias;
    $def['left_field'] = $this->real_field;
    if (!empty($this->options['required'])) {
      $def['type'] = 'INNER';
    }

    if (!empty($this->definition['extra'])) {
      $def['extra'] = $this->definition['extra'];
    }

    if (!empty($def['join_handler']) && class_exists($def['join_handler'])) {
      $join = new $def['join_handler'];
    }
    else {
      $join = new views_join();
    }
    $join->definition = $def;
    $join->options = $this->options;
    $join->construct();
    $join->adjusted = TRUE;

    // Filter by period
    $join->extra = array(
      array(
        'field' => 'period',
        'operator' => '=',
        'value' => $this->options['period'],
      ),
    );

    // use a short alias for this:
    $alias = $def['table'] . '_' . $this->table;

    $this->alias = $this->query->add_relationship($alias, $join, $this->definition['base'], $this->relationship);

    // Add access tags if the base table provide it.
    if (empty($this->query->options['disable_sql_rewrite']) && isset($table_data['table']['base']['access query tag'])) {
      $access_tag = $table_data['table']['base']['access query tag'];
      $this->query->add_tag($access_tag);
    }
  }
}
