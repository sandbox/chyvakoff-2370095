<?php
/**
 * @file
 * Implements views.inc with hook_views_data.
 */

/**
 * Implements hook_views_data().
 */
function yandex_most_popular_views_data() {
  $data = array();

  foreach (entity_get_info() as $name => $type) {
    if ($name == 'node') {
      $data[$type['base table']]['yandex_most_popular_entity'] = array(
        'title' => t('Yandex most popular'),
        'help' => t('Yandex most popular widget.'),
        'field' => array(
          'handler' => 'yandex_most_popular_handler_field_yandex_most_popular',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'yandex_most_popular_handler_sort_yandex_most_popular',
        ),
        'filter' => array(
          'handler' => 'yandex_most_popular_handler_filter_yandex_most_popular',
          'allow empty' => TRUE,
        ),

        'relationship' => array(
          'handler' => 'yandex_most_popular_handler_relationship_yandex_most_popular',
          'base' => 'yandex_most_popular_entity',
          'base field' => 'entity_id',
          'relationship field' => 'nid',
          'title' => t('Most popular content'),
          'label' => t('Yandex most popular entities.'),
        ),
      );
    }
    if ($name == 'taxonomy_term') {
      $data[$type['base table']]['yandex_most_popular_entity'] = array(
        'title' => t('Yandex most popular'),
        'help' => t('Yandex most popular widget.'),
        'field' => array(
          'handler' => 'yandex_most_popular_handler_field_yandex_most_popular',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'yandex_most_popular_handler_sort_yandex_most_popular',
        ),
        'filter' => array(
          'handler' => 'yandex_most_popular_handler_filter_yandex_most_popular',
          'allow empty' => TRUE,
        ),
        'relationship' => array(
          'handler' => 'yandex_most_popular_handler_relationship_yandex_most_popular',
          'base' => 'yandex_most_popular_entity',
          'base field' => 'entity_id',
          'relationship field' => 'tid',
          'title' => t('Most popular taxonomy terms'),
          'label' => t('Yandex most popular entities.'),
        ),
      );
    }
  }

  return $data;
}
