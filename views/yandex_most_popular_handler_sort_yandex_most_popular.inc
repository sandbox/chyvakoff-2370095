<?php
/**
 * @file
 * Implements the sort handler class.
 */

class yandex_most_popular_handler_sort_yandex_most_popular extends views_handler_sort {
  function query() {
    if(isset($this->query->relationships['yandex_most_popular_entity_' . $this->query->base_table])) {
      $this->query->add_orderby('yandex_most_popular_entity_' . $this->query->base_table, 'views', $this->options['order']);
    }
  }
}
