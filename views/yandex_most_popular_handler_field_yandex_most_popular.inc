<?php
/**
 * @file
 * Implements the field handler class.
 */

class yandex_most_popular_handler_field_yandex_most_popular extends views_handler_field_entity {
  public function render($values) {
    return $values->yandex_most_popular_views;
  }

  public function query() {
    $this->ensure_my_table();
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();

    if(isset($this->query->relationships['yandex_most_popular_entity_' . $this->query->base_table])) {
      $this->field_alias = $this->query->add_field('yandex_most_popular_entity_' . $this->query->base_table, 'views', 'yandex_most_popular_views', $params);
    }

    $this->add_additional_fields();
  }
}
