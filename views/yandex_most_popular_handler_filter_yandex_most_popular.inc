<?php
/**
 * @file
 * Implements the filter handler class.
 */

class yandex_most_popular_handler_filter_yandex_most_popular extends views_handler_filter {
  public function option_definition() {
    $options = parent::option_definition();
    $options['period']['default'] = '';
    return $options;
  }

  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $operators = array(
      '<' => t('Is less than'),
      '>' => t('Is greater than'),
      '=' => t('Is equal to'),
      '!=' => t('Is not equal to'),
    );

    $form['operator'] = array(
      '#type' => 'select',
      '#title' => t('Operator'),
      '#options' => $operators,
      '#required' => TRUE,
      '#default_value' => $this->options['operator'],
    );

    $form['value'] = array(
      '#type' => 'textfield',
      '#size' => 30,
      '#required' => TRUE,
      '#default_value' => $this->options['value'],
    );
  }

  function query() {
    if(isset($this->query->relationships['yandex_most_popular_entity_' . $this->query->base_table])) {
      $this->query->add_where(
        $this->options['group'],
        'yandex_most_popular_entity_' . $this->query->base_table . '.views',
        (int) $this->options['value'],
        $this->options['operator']
      );
    }
  }

  function admin_summary() {
    return check_plain($this->options['period']) . $this->options['operator'] . $this->options['value'];
  }
}
