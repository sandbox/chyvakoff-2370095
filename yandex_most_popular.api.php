<?php
/*
------------------------------------------------------------------------------
Yandex Most Popular API (API DOCUMENTATION)
------------------------------------------------------------------------------
*/

/*
To get most popular entities you may use function yandex_most_popular_info().

Possible parameters:
- type of entity (node, term and others).
- period (day, week or month).
- storage (mysql, mssql or other storage)
- count of extracted entities

These parameters must be set at module settings page admin/config/content/yandex_most_popular
*/
function yandex_most_popular_info($entity_type = 'all', $period = 'all', $storage = 'database', $count = 'all') {

}
