<?php
/**
 * @file
 * Administration pages for the Yandex most popular module.
 */

function yandex_most_popular_settings() {
  $form = array();
  $form['yandex_most_popular_counter_id'] = array(
    '#title' => t('Counter ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Yandex Metrika counter ID.'),
    '#default_value' => variable_get('yandex_most_popular_counter_id', ''),
  );
  $form['yandex_most_popular_periods'] = array(
    '#title' => t('Periods'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#description' => t('Load data for specified periods. Each period on a new line.'),
    '#default_value' => variable_get('yandex_most_popular_periods', "day\nweek\nmonth"),
  );
  $form['yandex_most_popular_entities_count'] = array(
    '#title' => t('Entities count'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Count of rows with statistics from Yandex.Metrika (for every period).'),
    '#default_value' => variable_get('yandex_most_popular_entities_count', 30),
  );

  $plugins_storage = yandex_most_popular_get_storage();
  $storage_options = array();
  foreach ($plugins_storage as $storage_name => $storage_plugin) {
    $storage_options[$storage_name] = $storage_plugin['name'];
  }
  $form['yandex_most_popular_storage'] = array(
    '#title' => t('Storage'),
    '#type' => 'select',
    '#options' => $storage_options,
    '#required' => TRUE,
    '#description' => t('Storage for Yandex Metrika statistics.'),
    '#default_value' => variable_get('yandex_most_popular_storage', 'database'),
  );

  $plugins_entity_type = yandex_most_popular_get_entity_type();
  $entity_type_options = array();
  foreach ($plugins_entity_type as $entity_type_name => $entity_type_plugin) {
    $entity_type_options[$entity_type_name] = $entity_type_plugin['name'];
  }
  $form['yandex_most_popular_entity_type'] = array(
    '#title' => t('Entity types'),
    '#type' => 'checkboxes',
    '#options' => $entity_type_options,
    '#required' => TRUE,
    '#description' => t('All available "entity_type" ctools plugins. Check the entity types, which you want to save.'),
    '#default_value' => variable_get('yandex_most_popular_entity_type', array('node' => 'node', 'term' => 'term')),
  );
  return system_settings_form($form);
}
